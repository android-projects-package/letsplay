package com.letsplay.app;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.letsplay.app.Model.User;

public class SignUpActivity extends AppCompatActivity {

    EditText userName,userEmail,userPassword,userConfirmPassword,age,about;
    Button signUpButton;
    ProgressDialog progressDialog;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initViews();
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isDataProvided()){
                    progressDialog.show();
                    firebaseAuth.createUserWithEmailAndPassword(userEmail.getText().toString().trim(),
                            userPassword.getText().toString().trim())
                            .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult) {
                                    saveUserInformation();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    progressDialog.dismiss();
                                    Toast.makeText(SignUpActivity.this, "Exception: " +
                                            e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }
        });
    }

    private void saveUserInformation() {
        User user = new User(
                userName.getText().toString(),
                userEmail.getText().toString(),
                age.getText().toString(),
                about.getText().toString()
                ,"live"
        );

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users");
        databaseReference.child(userId).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    progressDialog.dismiss();
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    progressDialog.dismiss();
                    Toast.makeText(SignUpActivity.this, "Username could not be stored", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isDataProvided() {
        if(TextUtils.isEmpty(userName.getText())){
            userName.setError("Enter Username");
            return false;
        }
        else if(TextUtils.isEmpty(userEmail.getText())){
            userEmail.setError("Enter Email");
            return false;
        }
        else if(TextUtils.isEmpty(userPassword.getText())){
            userPassword.setError("Enter Password");
            return false;
        }
        else if(TextUtils.isEmpty(userConfirmPassword.getText())){
            userConfirmPassword.setError("Enter Password");
            return false;
        }
        else if(userPassword.getText().equals(userConfirmPassword.getText())){
            Toast.makeText(this, "Password should be same", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    private void initViews() {
        userName = findViewById(R.id.edusername);
        userEmail = findViewById(R.id.edemail);
        age = findViewById(R.id.edage);
        about = findViewById(R.id.about);

        userPassword = findViewById(R.id.edpass);
        userConfirmPassword = findViewById(R.id.edrepass);
        signUpButton = findViewById(R.id.button1);
        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("SignUp");
        progressDialog.setMessage("Creating account please wait....");
        progressDialog.setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
