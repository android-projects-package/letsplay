package com.letsplay.app;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
public class LoginActivity extends AppCompatActivity {

    Button signInButton,newUserButton,forgotPasswordButton;

    EditText userEmail,userPassword;

    ProgressDialog progressDialog;

    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        signInButton = findViewById(R.id.btn_SignIn);

        newUserButton = findViewById(R.id.btn_SignUp);

        userEmail = findViewById(R.id.user_email);

        userPassword = findViewById(R.id.user_pass);

        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);

        progressDialog.setTitle("User Login");

        progressDialog.setMessage("Signing in please wait....");

        progressDialog.setCancelable(false);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isDataProvided()){
                    progressDialog.show();
                    firebaseAuth.signInWithEmailAndPassword(userEmail.getText().toString().trim(),
                            userPassword.getText().toString().trim())
                            .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult) {
                                    progressDialog.dismiss();
                                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, "Exception: "
                                    +e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
        newUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),SignUpActivity.class);
                startActivity(intent);

            }
        });
    }

    private boolean isDataProvided() {

        if(TextUtils.isEmpty(userEmail.getText())){
            userEmail.setError("Please Enter Email");
            return false;
        }
        else if(TextUtils.isEmpty(userPassword.getText())){
            userPassword.setError("Please Enter Password");
            return false;
        }
        return true;

    }

}
