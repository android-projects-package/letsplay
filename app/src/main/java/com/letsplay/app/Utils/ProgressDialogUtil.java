package com.letsplay.app.Utils;
import android.app.ProgressDialog;
import android.content.Context;
public class ProgressDialogUtil {

    private ProgressDialog progressDialog;

    public ProgressDialogUtil(Context context, String title, String message, int icon){
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setIcon(icon);
    }

    public void showAlertDialog(){
        progressDialog.show();
    }

    public void stopAlertDialog(){
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }
}
