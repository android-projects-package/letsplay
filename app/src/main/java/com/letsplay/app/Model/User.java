package com.letsplay.app.Model;

public class User {
    private String username,email,age,about,status;
    public User(){}

    public User(String username, String email, String age, String about, String status) {
        this.username = username;
        this.email = email;
        this.age = age;
        this.about = about;
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", age='" + age + '\'' +
                ", about='" + about + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
