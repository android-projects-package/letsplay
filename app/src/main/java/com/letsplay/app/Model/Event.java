package com.letsplay.app.Model;

public class Event {
    private String organizerName,eventDescription,time;
    public Event(){}

    public Event(String organizerName, String eventDescription, String time) {
        this.organizerName = organizerName;
        this.eventDescription = eventDescription;
        this.time = time;
    }

    public String getOrganizerName() {
        return organizerName;
    }

    public void setOrganizerName(String organizerName) {
        this.organizerName = organizerName;
    }


    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Event{" +
                "organizerName='" + organizerName + '\'' +
                ", eventDescription='" + eventDescription + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
