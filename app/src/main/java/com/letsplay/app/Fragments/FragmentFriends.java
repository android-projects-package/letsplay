package com.letsplay.app.Fragments;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.letsplay.app.Adapter.UsersAdpater;
import com.letsplay.app.Model.User;
import com.letsplay.app.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentFriends extends Fragment {

    private List<User> userList;
    private RecyclerView recyclerView;
    ProgressDialog progressDialog;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    public FragmentFriends() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater mInflater = LayoutInflater.from(getActivity());
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView titleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
        TextView centerTextView = (TextView) mCustomView.findViewById(R.id.centerText);
        //titleTextView.setText("POST");
        centerTextView.setText("Let's Play");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(mCustomView);

        /*titleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity(), "Friends Fragment Called", Toast.LENGTH_SHORT).show();

            }
        });*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_friends, container, false);

        userList = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("users");
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Users");
        progressDialog.setMessage("Getting Users...");
        progressDialog.setCancelable(true);
        progressDialog.show();

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){

                    for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                        User user = snapshot.getValue(User.class);
                        userList.add(user);
                    }

                    progressDialog.dismiss();
                    UsersAdpater usersAdpater = new UsersAdpater(getContext(),userList);
                    recyclerView.setAdapter(usersAdpater);
                    usersAdpater.notifyDataSetChanged();
                }
                else{
                    Toast.makeText(getContext(), "No Record Found!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getContext(), "Exception: "+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }
}