package com.letsplay.app.Fragments;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.letsplay.app.Model.Event;
import com.letsplay.app.R;

public class AddEventFragment extends Fragment {

    EditText placeEditText,timeEditText,sportsEditText;
    Button postButton;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseUser firebaseUser;
    String userId = null;
    public AddEventFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view  = inflater.inflate(R.layout.fragment_add_event, container, false);


       firebaseDatabase = FirebaseDatabase.getInstance();

       databaseReference = firebaseDatabase.getReference("Events");


       placeEditText = view.findViewById(R.id.place);
       timeEditText = view.findViewById(R.id.time);
       sportsEditText = view.findViewById(R.id.sports);
       postButton = view.findViewById(R.id.postButton);
       postButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if(TextUtils.isDigitsOnly(placeEditText.getText())){
                   Toast.makeText(getContext(), "Please Provide Place To Play", Toast.LENGTH_SHORT).show();
               }
               else if(TextUtils.isEmpty(timeEditText.getText())){
                   Toast.makeText(getContext(), "Please Provide Time", Toast.LENGTH_SHORT).show();
               }
               else if(TextUtils.isEmpty(sportsEditText.getText())){
                   Toast.makeText(getContext(), "Please Provide Sport Type ", Toast.LENGTH_SHORT).show();
               }
               else{

                   final ProgressDialog progressDialog = new ProgressDialog(getContext());
                   progressDialog.setTitle("Information");
                   progressDialog.setMessage("Storing Event....");
                   progressDialog.show();
                   String place = placeEditText.getText().toString();
                   String time = timeEditText.getText().toString();
                   String sports = sportsEditText.getText().toString();
                   Event event = new Event("Moiz","Does anybody want to play "+sports+" at "+place+" in "+time,time);
                   
                   databaseReference.push().setValue(event).addOnSuccessListener(new OnSuccessListener<Void>() {
                       @Override
                       public void onSuccess(Void aVoid) {

                           if(progressDialog!=null && progressDialog.isShowing()){
                               progressDialog.dismiss();
                           }
                           Toast.makeText(getContext(), "Event Created Successfully! ", Toast.LENGTH_SHORT).show();
                           clearFormFields();
                           // TODO COMPLETE FURTHER WORK.
                           
                       }
                   }).addOnFailureListener(new OnFailureListener() {
                       @Override
                       public void onFailure(@NonNull Exception e) {
                           Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                       }
                   });
               }
           }
       });
       return view;
    }

    private void clearFormFields() {
        placeEditText.setText("");
        timeEditText.setText("");
        sportsEditText.setText("");
    }
}