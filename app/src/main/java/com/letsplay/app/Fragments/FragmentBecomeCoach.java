package com.letsplay.app.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.letsplay.app.R;

import java.util.HashMap;
import java.util.Map;


public class FragmentBecomeCoach extends Fragment {

    Switch aSwitch;
    FirebaseDatabase firebaseDatabase;
    FirebaseAuth firebaseAuth;
    DatabaseReference databaseReference;

    Map<String,String> map = new HashMap<>();
    ProgressDialog progressDialog;
    String role;



    public FragmentBecomeCoach() {

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater mInflater = LayoutInflater.from(getActivity());
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView titleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
        TextView centerTextView = (TextView) mCustomView.findViewById(R.id.centerText);

        centerTextView.setText("Let's Play");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(mCustomView);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_become_coach, container, false);
        aSwitch = view.findViewById(R.id.coach_switch);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Role").child(firebaseAuth.getCurrentUser().getUid());
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Role");
        progressDialog.setMessage("Setting Your Role");
        progressDialog.setCancelable(false);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    HashMap<String,String> hashMap = (HashMap<String, String>) dataSnapshot.getValue();
                    if(hashMap.get("role").equals("coach")){
                        aSwitch.setChecked(true);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getContext(), "Exception: "+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                progressDialog.show();

                if(b == true){ // checked
                    map.put("role","coach");
                    role = "coach";
                }
                else{
                    map.put("role","normalUser");
                    role = "normalUser";
                }

                databaseReference.setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if(task.isSuccessful()){
                            progressDialog.dismiss();
                            if(role.equals("coach")){
                                Toast.makeText(getContext(), "You are now a coach!!", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(getContext(), "You are not a coach anymore!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else{
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), "Exception: "+task.getException(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });


            }
        });

        return view;
    }


}