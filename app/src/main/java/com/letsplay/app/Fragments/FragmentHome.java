package com.letsplay.app.Fragments;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.letsplay.app.Adapter.EventsAdapter;
import com.letsplay.app.Model.Event;
import com.letsplay.app.Model.User;
import com.letsplay.app.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentHome extends Fragment {

    private List<Event> eventList;

    private RecyclerView recyclerView;

    ProgressDialog progressDialog;

    FirebaseDatabase firebaseDatabase;

    DatabaseReference databaseReference;

    DatabaseReference statusReference;

    public FragmentHome() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        LayoutInflater mInflater = LayoutInflater.from(getActivity());
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        View view = mInflater.inflate(R.layout.fragment_home,null);
        TextView titleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
        TextView centerTextView = (TextView) mCustomView.findViewById(R.id.centerText);
        //titleTextView.setText("POST");
        centerTextView.setText("FEED");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(mCustomView);



       /* titleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity(), "Home Fragment Called", Toast.LENGTH_SHORT).show();

            }
        });*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        statusReference = FirebaseDatabase.getInstance().getReference(
                "users"
        ).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        statusReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    User user = dataSnapshot.getValue(User.class);
                    user.setStatus("online");
                    statusReference.setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(!task.isSuccessful()){
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), "Unable to set status to Online", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });



        /*Map<String,String> map = new HashMap<>();
        map.put("status","online");
        statusReference.setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(getContext(), "Status is Live", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getContext(), "Could not update your status", Toast.LENGTH_SHORT).show();
                }
            }
        });*/


        eventList = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Events");
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Events");
        progressDialog.setMessage("Getting Events Details");
        progressDialog.setCancelable(true);
        progressDialog.show();


        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){

                    for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                        Event event = snapshot.getValue(Event.class);
                        eventList.add(event);
                    }

                    progressDialog.dismiss();
                    EventsAdapter eventsAdapter = new EventsAdapter(getContext(),eventList);
                    recyclerView.setAdapter(eventsAdapter);
                    eventsAdapter.notifyDataSetChanged();
                }
                else{
                    Toast.makeText(getContext(), "No Record Found!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getContext(), "Exception: "+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }


}