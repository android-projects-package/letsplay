package com.letsplay.app;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DividerItemDecoration;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.letsplay.app.Fragments.EventsFragment;
import com.letsplay.app.Fragments.FragmentBecomeCoach;
import com.letsplay.app.Fragments.FragmentFriends;
import com.letsplay.app.Fragments.FragmentHelp;
import com.letsplay.app.Fragments.FragmentHome;
import com.letsplay.app.Model.User;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    TextView centerTextView,titleTextView;
    ProgressDialog progressDialog;
    TextView userName,userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_drawer_layout);
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle("Alert");
        progressDialog.setMessage("Closing Application");
        progressDialog.setCancelable(false);
        Toolbar toolbar = findViewById(R.id.toolbar);
        // TO SET THE COLOR OF ACTION BAR TEXT
        toolbar.setTitleTextColor(getResources().getColor(R.color.black));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerLayout  = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.navigation_view);

        View hView =  navigationView.getHeaderView(0);
        userName = hView.findViewById(R.id.name);
        userEmail = hView.findViewById(R.id.email);

        // this line of code is used to set the colorful images for drawer items. without this colorless image would appear
        navigationView.setItemIconTintList(null);

        // these two lines of code is used to create a divider line for nav items in the drawer.
        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
        navMenuView.addItemDecoration(new DividerItemDecoration(MainActivity.this,DividerItemDecoration.VERTICAL));



        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,drawerLayout,R.string.open_navigation_drawer,R.string.close_navigation_drawer);


        // TO CHANGE THE COLOR OF HAMBURGER ICON

        actionBarDrawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.black));
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        titleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
        centerTextView = (TextView) mCustomView.findViewById(R.id.centerText);

        //titleTextView.setText("post");
        //centerTextView.setText("feed");

        getSupportActionBar().setCustomView(mCustomView);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        //getSupportActionBar().setTitle("Feed");

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        getUserProfileInformation();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container,new FragmentHome()).commit();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(Gravity.START)){
            drawerLayout.closeDrawer(Gravity.START);
        }
        else{
            super.onBackPressed();
        }
            /*progressDialog.show();
            final DatabaseReference statusReference = FirebaseDatabase.getInstance().getReference(
                    "users"
            ).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            statusReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        User user = dataSnapshot.getValue(User.class);
                        user.setStatus("offline");
                        statusReference.setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    progressDialog.dismiss();
                                    MainActivity.super.onBackPressed();
                                }
                                else{
                                    progressDialog.dismiss();
                                    Toast.makeText(MainActivity.this, "Unable to set status to Offline", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            //super.onBackPressed();
        }*/
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {



        switch(menuItem.getItemId()){


            case R.id.home:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,new FragmentHome()).commit();
                break;

            case R.id.events:
                // add new fragment here
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,new EventsFragment()).commit();
                

//                centerTextView.setText("Events");
              /*  titleTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new FragmentSettings()).commit();
                        Toast.makeText(MainActivity.this, "POST ACTION CLICKED", Toast.LENGTH_SHORT).show();
                    }
                });*/
                break;

            case R.id.friends:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,new FragmentFriends()).commit();
                break;

            /*case R.id.map:
                // add new fragment here
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,new FragmentMaps()).commit();
                break;*/

            case R.id.becomeCoach:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,new FragmentBecomeCoach()).commit();
                break;

            case R.id.help:
                // add new fragment here
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,new FragmentHelp()).commit();
                break;

            case R.id.logout:

                progressDialog.show();
            final DatabaseReference statusReference = FirebaseDatabase.getInstance().getReference(
                    "users"
            ).child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            statusReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()){
                        User user = dataSnapshot.getValue(User.class);
                        user.setStatus("offline");
                        statusReference.setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    progressDialog.dismiss();
                                    FirebaseAuth.getInstance().signOut();
                                    Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                else{
                                    progressDialog.dismiss();
                                    Toast.makeText(MainActivity.this, "Unable to set status to Offline", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            //super.onBackPressed();
        break;

            /*case R.id.settings:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container,new FragmentSettings()).commit();
                break;*/
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    /* this function is used to get the user account information from the firebase */
    public void getUserProfileInformation(){

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    User user = dataSnapshot.getValue(User.class);
                    userName.setText(user.getUsername());
                    userEmail.setText(user.getEmail());
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Toast.makeText(MainActivity.this, "Exception "+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }



}
