package com.letsplay.app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.letsplay.app.Model.User;
import com.letsplay.app.R;

import java.util.List;

public class UsersAdpater extends RecyclerView.Adapter<UsersAdpater.UsersViewHolder> {
    private Context context;
    private List<User> userList;

    public UsersAdpater(Context context,List<User> userList){
        this.context = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_users,parent,false);
        return new UsersViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull UsersViewHolder holder, int position) {
        User user = userList.get(position);
        holder.username.setText(user.getUsername());
        holder.age.setText("Age: "+user.getAge());
        holder.about.setText(user.getAbout());
        holder.status.setText(user.getStatus());
    }
    @Override
    public int getItemCount() {
        return userList.size();
    }
    public class UsersViewHolder extends RecyclerView.ViewHolder {
        TextView username,status,age,about;
        public UsersViewHolder(@NonNull View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.username);
            status = itemView.findViewById(R.id.status);
            age = itemView.findViewById(R.id.age);
            about = itemView.findViewById(R.id.about);
        }
    }
}
