package com.letsplay.app.Adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.letsplay.app.Model.Event;
import com.letsplay.app.R;
import java.util.List;
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsViewHolder> {

    private Context context;
    private List<Event> eventList;

    public EventsAdapter(Context context,List<Event> eventList){
        this.context = context;
        this.eventList = eventList;
    }

    @NonNull
    @Override
    public EventsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_event,parent,false);
        return new EventsViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull EventsViewHolder holder, int position) {

        Event event = eventList.get(position);
        holder.eventTitle.setText("Invitation From "+event.getOrganizerName());
        holder.eventTime.setText(event.getTime());
        holder.eventDescription.setText(event.getEventDescription());

    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public class EventsViewHolder extends RecyclerView.ViewHolder {
        TextView eventTitle,eventDescription,eventTime;
        public EventsViewHolder(@NonNull View itemView) {
            super(itemView);
            eventTitle = itemView.findViewById(R.id.eventTitle);
            eventDescription = itemView.findViewById(R.id.eventDescription);
            eventTime = itemView.findViewById(R.id.time);
        }
    }
}
